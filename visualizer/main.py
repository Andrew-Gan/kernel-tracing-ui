#!/usr/bin/python3

################################################
# visualizer for kernel tracing
# note: ensure no blank lines exist in log file
################################################

import sys
import matplotlib.pyplot as plt
import interactive_ui as inter

# configuration:

"""
tid_color       - colors to represent operations by different threads
read_color      - color for representing read operations
write_color     - color for representing write operations
box_color       - color for filling in rectangles that represent an interval
part_color      - color for partitioning the rows
dis_group       - registers to display. Use format: (token index, string)
tFilterOn       - if set to true, only traces within tFilter range are plotted
tFilter         - range of timestamp to focus on
rec_group       - registers to record interval (index, name, starting, ending)
"""

tid_color = 'gycmk'
read_color = 'b'
write_color = 'r'
box_color = 'c'
part_color = 'w'
tFilterOn = True
tFilter = (615239000000, 615245000000)
dis_group = ((8, 'CPU_CTRL'), (8, 'JOB_CTRL'), (8, 'MEM_MGMT'),
             (2, 'SYSCALL', 'IOCTL', 'FOPS'))
rec_group = (  (2, 'JIN_PM'      , 'JIN_PM_START', 'JIN_PM_DONE'), 
               (2, 'SUBMIT_IRQ'  , 'JM_SUBMIT,'  , 'JM_IRQ,'    ))
               
# source code:

"""
Given the path to a trace log file, return the tokenized entries of the file.
The output is a 2d list with each element representing a tokenized entry.
"""

def parseLog(filename):
    fp = open(sys.argv[1], 'r')
    entries = []
    newline = fp.readline()
    while(newline):
        entries.append(newline.strip().split())
        newline = fp.readline()
    fp.close()
    return entries

"""
Check if the line in trace file needs to be plotted and returns a 2d tuple with
format (a, b) where a: 0 - vertical line, 1 - rectangle; b: index in tuple
"""

def checkGroup(entry):
    # check if trace log falls within specified timeframe
    currTime = int(entry[0])
    if tFilterOn and ((currTime < tFilter[0]) or (currTime > tFilter[1])):
        return -1, -1
    # search for entry in dis_group
    for i in range(len(dis_group)):
        if len(entry) > dis_group[i][0]:
            for j in range(1, len(dis_group[i])):
                if dis_group[i][j] in entry[dis_group[i][0]]:
                    return 0, i
    # search for entry in rec_group
    for i in range(len(rec_group)):
        if len(entry) <= rec_group[i][0]:
            break
        if rec_group[i][2] in entry[rec_group[i][0]]:
            return 1, i
        elif rec_group[i][3] in entry[rec_group[i][0]]:
            return 1, i
    # trace log does not need to be plotted
    return -1, -1

"""
Receive a trace log and index, retrieve recorded info and plot a rectangle.
Any data recorded during the interval are stored in plotRect.boxes
(bottom left, top right, number of rw, thread id)
"""

def plotRect(entry, i):
    timestamp = int(entry[0]) / 1000000
    r = rec_group[i]
    if r[2] in entry[r[0]]:
        plotRect.startTimes[r[1]] = (timestamp, plotRect.numRW)
    if r[3] in entry[r[0]]:
        if entry[4] not in plotRect.tid:
            plotRect.tid.append(entry[4])
        botleftP = (plotRect.startTimes[r[1]][0], len(dis_group)+i-0.5)
        rect_w = timestamp - plotRect.startTimes[r[1]][0]
        colorcode = tid_color[plotRect.tid.index(entry[4])]
        rect = plt.Rectangle(botleftP, rect_w, 1, color=colorcode)
        plt.gca().add_patch(rect)
        numRW_intrv = plotRect.numRW - plotRect.startTimes[r[1]][1]
        toprightP = (timestamp, botleftP[1]+1)
        plotRect.boxes.append((botleftP, toprightP, numRW_intrv, entry[4]))

plotRect.numRW = 0
plotRect.startTimes = {}
plotRect.boxes = []
plotRect.tid = []

"""
Given a list containing tokenized entries from a trace file, plot the
verticle lines or rectangles representing each entry on a graph.
"""

def plotLines(entries):
    for e in entries:
        time = int(e[0]) / 1000000
        ret = checkGroup(e)
        if ret[0] == 0:
            plotRect.numRW += 1
            row = ret[1]
            if e[3] == 'R:':
                plt.vlines(time, row-0.5, row+0.5, read_color)
            else:
                plt.vlines(time, row-0.5, row+0.5, write_color)
        if ret[0] == 1:
            plotRect(e, ret[1]) 
    plt.axhline((-0.5), linewidth=30, color=part_color)
    for i in range(len(dis_group) + len(rec_group)):
        plt.axhline((i+0.5), linewidth=30, color=part_color)

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('Usage: python vlines.py <path to trace log>')
        exit()
    entries = parseLog(sys.argv[1])
    plotLines(entries)
    inter.initCursorInteraction(plotRect.boxes, plt.gcf(), plt.gca())
    plt.xlabel('Timestamp (ms)')
    plt.grid()
    regs_to_plot = [r[1] for r in dis_group] + [r[1] for r in rec_group]
    plt.yticks(range(0, len(regs_to_plot)), regs_to_plot)
    plt.gcf().canvas.set_window_title('Kernel Tracer Visualization Tool')
    plt.show()
    
