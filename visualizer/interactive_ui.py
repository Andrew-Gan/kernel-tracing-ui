################################################################################
# interactive ui for plots
# adds zooming, scrolling and cursor hover annotation to a matplotlib plot
# ref:
# https://matplotlib.org/3.2.1/api/_as_gen/matplotlib.axes.Axes.set_xlim.html
# https://matplotlib.org/3.2.1/api/backend_bases_api.html
# https://matplotlib.org/3.2.1/api/_as_gen/matplotlib.pyplot.text.html
# https://matplotlib.org/3.2.1/users/event_handling.html
################################################################################

import matplotlib.pyplot as plt

def initCursorInteraction(boxes, fig, ax):
    tid_text = fig.text(0.3, 0.95, "")
    dur_text = fig.text(0.3, 0.925, "")
    rw_text = fig.text(0.3, 0.9, "")

    def checkCursorInArea(botleftP, toprightP, cursorP):
        cond = 0
        if (botleftP[0] < cursorP[0]) and (toprightP[0] > cursorP[0]):
            cond += 1   # horizontal within range
        if (botleftP[1] < cursorP[1]) and (toprightP[1] > cursorP[1]):
            cond += 1   # vertical within range
        return cond == 2

    def on_cursor_move(event):
        if None not in (event.xdata, event.ydata):
            for b in boxes:
                cursorPos = (event.xdata, event.ydata)
                if checkCursorInArea(b[0], b[1], cursorPos):
                    tid_text.set_text(b[3][:-1])
                    duration = b[1][0] - b[0][0]
                    dur_text.set_text("duration: {:.2f} ms".format(duration))
                    rw_text.set_text("rw count: {}".format(b[2]))
                    fig.canvas.draw()
                    break

    def on_scroll(event):
        new_xlim = ax.get_xlim()
        diff_xlim = new_xlim[1] - new_xlim[0]
        if event.button == 'up':
            new_xlim = (new_xlim[0]+diff_xlim*0.1, new_xlim[1]-diff_xlim*0.1)
            ax.set_xlim(new_xlim)
            plt.draw()
        elif event.button == 'down':
            new_xlim = (new_xlim[0]-diff_xlim*0.1, new_xlim[1]+diff_xlim*0.1)
            ax.set_xlim(new_xlim)
            plt.draw()
    
    def on_key_press(event):
        new_xlim = ax.get_xlim()
        diff_xlim = (new_xlim[1] - new_xlim[0]) * 0.1
        if event.key == 'left' or event.key == 'a':
            new_xlim = (new_xlim[0] - diff_xlim, new_xlim[1] - diff_xlim)
            ax.set_xlim(new_xlim)
            plt.draw()
        elif event.key == 'right' or event.key == 'd':
            new_xlim = (new_xlim[0] + diff_xlim, new_xlim[1] + diff_xlim)
            ax.set_xlim(new_xlim)
            plt.draw()
        
    fig.canvas.mpl_connect('motion_notify_event', on_cursor_move)
    fig.canvas.mpl_connect('scroll_event', on_scroll)
    fig.canvas.mpl_connect('key_press_event', on_key_press)
