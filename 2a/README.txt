Platform:
Ubuntu 20.04 LTS
gcc 9.3.0
Intel Core i5-7200U CPU @ 2.50GHz × 4

Part 0. Instruction count per context switch
Part 1. Cpu cycles and cache misses per context switches

Summary: 
Using the perf api for recording cpu stats and ioctl for making system call I/O
requests, the number of instructions when switching execution from one thread to
another was recorded. Three perf events were created to record the instruction
count, cpu cycles and cache misses. The parent process first creates a child
process. Then a system call was made to begin recording the perf events, and 
another system call was performed in the child process to stop recording.

Building and execution:
make
make run

Measurement:
The process was repeated 1000 times and the summation of the recorded values in
the file descriptors of the three perf events were taken and averaged to obtain
the average number of the three events over 1000 context switches.