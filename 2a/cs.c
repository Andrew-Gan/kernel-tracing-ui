#include <stdio.h>
#include <pthread.h>
#include <time.h>
#include <string.h>
#include <linux/perf_event.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <asm/unistd.h>

typedef struct AttrRetr {
    long ins_count, cpu_count, che_count;
} AttrRetr;

void* runnable(void* arg) {
    int fd_ins = ((long*)arg)[0];
    int fd_cpu = ((long*)arg)[1];
    int fd_che = ((long*)arg)[2];
    
    ioctl(fd_ins, PERF_EVENT_IOC_DISABLE, 0);
    ioctl(fd_cpu, PERF_EVENT_IOC_DISABLE, 0);
    ioctl(fd_che, PERF_EVENT_IOC_DISABLE, 0);
    
    return NULL;
}

static long
perf_event_open(struct perf_event_attr* attr, pid_t pid, int cpu, 
                int group_fd, unsigned long flags) {
    int ret = syscall(__NR_perf_event_open, attr, pid, cpu, group_fd, flags);

    return ret;
}

int main() {
    struct perf_event_attr pe_attr_ins, pe_attr_cpu, pe_attr_che;
    void* instr_count;
    pthread_t child_pid;
    AttrRetr totalRetr;
    int rounds = 1000;

    memset(&totalRetr, 0, sizeof(totalRetr));
    memset(&pe_attr_ins, 0, sizeof(pe_attr_ins));
    pe_attr_ins.disabled = 1;
    pe_attr_ins.exclude_kernel = 1;
    pe_attr_ins.size = sizeof(struct perf_event_attr);
    pe_attr_ins.type = PERF_TYPE_HARDWARE;
    pe_attr_cpu = pe_attr_ins;
    pe_attr_che = pe_attr_ins;
    
    pe_attr_ins.config = PERF_COUNT_HW_INSTRUCTIONS;
    pe_attr_cpu.config = PERF_COUNT_HW_CPU_CYCLES;
    pe_attr_che.config = PERF_COUNT_HW_CACHE_MISSES;

    int fd_ins = perf_event_open(&pe_attr_ins, 0, -1, -1, 0);
    int fd_cpu = perf_event_open(&pe_attr_cpu, 0, -1, -1, 0);
    int fd_che = perf_event_open(&pe_attr_che, 0, -1, -1, 0);
    if((fd_ins == -1) || (fd_cpu == -1) || (fd_che == -1)) {
        printf("Error: Unable to open perf event\n");
        return 1;
    }

    AttrRetr myretr;
    long arg[3] = {fd_ins, fd_cpu, fd_che};
    
    for(int i = 0; i < rounds; i++) {
        ioctl(fd_ins, PERF_EVENT_IOC_RESET, 0);
        ioctl(fd_cpu, PERF_EVENT_IOC_RESET, 0);
        ioctl(fd_che, PERF_EVENT_IOC_RESET, 0);

        pthread_create(&child_pid, NULL, &runnable, arg);

        ioctl(fd_ins, PERF_EVENT_IOC_ENABLE, 0);
        ioctl(fd_cpu, PERF_EVENT_IOC_ENABLE, 0);
        ioctl(fd_che, PERF_EVENT_IOC_ENABLE, 0);

        pthread_join(child_pid, NULL);

        read(fd_ins, &myretr.ins_count, sizeof(long));
        read(fd_cpu, &myretr.cpu_count, sizeof(long));
        read(fd_che, &myretr.che_count, sizeof(long));

        totalRetr.ins_count += myretr.ins_count;
        totalRetr.cpu_count += myretr.cpu_count;
        totalRetr.che_count += myretr.che_count;
    }

    printf("instruction count: %ld\n", totalRetr.ins_count / rounds);
    printf("cpu cycles: %ld\n", totalRetr.cpu_count / rounds);
    printf("missed caches: %ld\n", totalRetr.che_count / rounds);
    close(fd_ins);
    close(fd_cpu);
    close(fd_che);
}