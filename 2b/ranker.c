#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

typedef enum findMode {
    MIN, MAX
} findMode;

typedef struct IntegerProfile {
    int val;
    long freq;
} IntegerProfile;

int find_maxmin(IntegerProfile* arr, unsigned int size, findMode mode) {
    IntegerProfile thres_prof = arr[0];
    int currIdx = 0;
    for(int i = 1; i < size; i++) {
        if((mode == MIN && arr[i].freq < thres_prof.freq) || (mode == MAX && arr[i].freq > thres_prof.freq)) {
            thres_prof = arr[i];
            currIdx = i;
        }
    }
    return currIdx;
}

void print_order(IntegerProfile* arr, unsigned int size, findMode order) {
    for(int i = 0; i < size; i++) {
        int idx_to_print = find_maxmin(arr, size, order);
        fprintf(stderr, "%d [%ld] ", arr[idx_to_print].val, arr[idx_to_print].freq);
        arr[idx_to_print].freq = -1;
    }
}

int find_element(IntegerProfile* arr, unsigned int size, int val) {
    for(int i = 0; i < size; i++) {
        if(arr[i].val == val) return i;
    }
    return -1;
}

void
arrange_list(unsigned int curr_index, unsigned int* all_freq_count,
            IntegerProfile* most_freq_count, unsigned int size) {
    int thres_list_idx = find_maxmin(most_freq_count, size, MIN);
    IntegerProfile thres_profile = most_freq_count[thres_list_idx];
    if(all_freq_count[curr_index] > thres_profile.freq) {
        int ret = find_element(most_freq_count, size, curr_index);
        // update freq of at most kth frequent value in most_freq_count
        if(ret != -1) {
            most_freq_count[ret].freq = all_freq_count[curr_index];
        }
        // register newly found at most kth frequent value in most_freq_count
        else {
            most_freq_count[thres_list_idx].val = curr_index;
            most_freq_count[thres_list_idx].freq = all_freq_count[curr_index];
        }
    }
}

int main(int argc, char** argv) {
    if (argc < 3) {
        printf("Usage: ./ranker <m seconds to record> <k frequent integers>\n");
        return EXIT_FAILURE;
    }
    int m_rec = atoi(argv[1]);
    int k_int = atoi(argv[2]);

    unsigned int all_freq_count[20001] = {0};
    time_t prev_time = 0, curr_time = 0;
    IntegerProfile* most_freq_count = calloc(k_int, sizeof(IntegerProfile));
    char read_str[10];
    
    while(1) {
        for(curr_time = 0; (curr_time - prev_time) < (m_rec * CLOCKS_PER_SEC); curr_time = clock()) {
            fgets(read_str, 10, stdin);
            int read_val_idx = atoi(read_str) + 10000;
            all_freq_count[read_val_idx] += 1;
            arrange_list(read_val_idx, all_freq_count, most_freq_count, k_int);
        }
        prev_time = curr_time;
        fprintf(stderr, "ranker: top %d: ", k_int);
        print_order(most_freq_count, k_int, MAX);
        fprintf(stderr, "in %d secs\n", m_rec);
        memset(all_freq_count, 0, sizeof(all_freq_count));
        memset(most_freq_count, 0, k_int * sizeof(IntegerProfile));
    }
    return EXIT_SUCCESS;
}