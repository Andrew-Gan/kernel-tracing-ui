typedef struct CircBuffer {
    int val[100];
    unsigned int count;
    unsigned int start, end;
} CircBuffer;

void initCircBuffer(CircBuffer* buff);
void pushElement(CircBuffer* buff, int val, int* exit_status);
int popElement(CircBuffer* buff, int* exit_status);