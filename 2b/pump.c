#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char** argv) {
    if(argc < 2) {
        printf("Usage: ./pump <duration of throughput>");
        return EXIT_FAILURE;
    }
    int intrvl_sec = atoi(argv[1]);
    int intrvl_cycle = intrvl_sec * CLOCKS_PER_SEC;
    time_t prevTime = 0;
    time_t currTime = 0;
    size_t numBytes = 0;
    
    while(1) {
        for(currTime=0; (currTime-prevTime) < intrvl_cycle; currTime=clock()) {
            numBytes += fprintf(stdout, "%d\n", (rand() % 20001) - 10000);
        }
        prevTime = currTime;
        float out_rate = (float)numBytes / 1000000 / intrvl_sec;
        fprintf(stderr, "pump: %0.2f MB/s in %d secs\n", out_rate, intrvl_sec);
        numBytes = 0;
    }
    return EXIT_SUCCESS;
}