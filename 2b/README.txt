Platform:
Ubuntu 20.04 LTS
gcc 9.3.0
Intel Core i5-7200U CPU @ 2.50GHz × 4

Part 0. pump

Summary:
The C random() and modulus operation were used for generating random numbers.
For a specified duration, the numbers were generated and printed to stderr.
During that time, the return value of fprintf was summed up for calculating the
average throughput over the period of time.
The entire process was contained in an infinite loop.

Building and execution:
make
make testpump

Measurement:
Sum up the return value of fprintf to obtain number of bytes written.
The value was summed up and divided by the time interval.
The summed up value was then reset to 0 for the next time interval.

Part 1. ranker

Summary:
A frequency array kept track of frequencies of all generated integers, and a
short array recorded the k most frequent integers. The program first received
input from stdin and converted the value to its index in the frequency array to
be incremented. Then, the program checked if the new frequency was greater than
the kth most frequent value. If so, the new value replaces the kth most frequent
value in the short list and a new kth most frequent integer was searched.

Building and execution:
make
make run

Measurement:
The k most frequent characters were printed every m seconds by obtaining the
short list and printing in descending order.

Part 2. multicore ranker

Summary:
The responsibilities of the program were divided into three parts:
i. Reading the integers from stdin
ii. updating the frequency array and short list
iii. printing the k most frequent integers to stderr every m seconds
Three threads were created for these three child processes.
The IPC between i and ii were conducted using a circular buffer and conditional
variables. The main process halted until all three threads have finished (never)

Build and execution:
make
make core

Measurement:
- Same as part 2 -