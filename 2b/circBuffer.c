#include <stdio.h>
#include "circBuffer.h"

void initCircBuffer(CircBuffer* buff) {
    buff->start = 0;
    buff->end = -1;
    buff->count = 0;
}

void pushElement(CircBuffer* buff, int val, int* exit_status) {
    if(buff->count >= 100) {
        if(exit_status != NULL) *exit_status = -1;
    }
    else {
        buff->count++;
        buff->end = (buff->end + 1) % 100;
        buff->val[buff->end] = val;
        if(exit_status != NULL) *exit_status = 0;
    }
}

int popElement(CircBuffer* buff, int* exit_status) {
    int ret = 0;
    if(buff->count <= 0) {
        if(exit_status != NULL) *exit_status = -1;
    }
    else {
        buff->count--;
        ret = buff->val[buff->start];
        buff->start = (buff->start + 1) % 100;
        if(exit_status != NULL)  *exit_status = 0;
    }
    
    return ret;
}