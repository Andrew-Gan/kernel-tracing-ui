#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include "circBuffer.h"

typedef enum findMode {
    MIN, MAX
} findMode;

typedef struct IntegerProfile {
    int val;
    long freq;
} IntegerProfile;

unsigned int wholelist[20001] = {0};
IntegerProfile* shortlist = NULL;
int m_rec = 0;
int k_int = 0;

int find_maxmin(IntegerProfile* arr, unsigned int k_int, findMode mode) {
    IntegerProfile thres_prof = arr[0];
    int currIdx = 0;
    for(int i = 1; i < k_int; i++) {
        if((mode == MIN && arr[i].freq < thres_prof.freq) || 
           (mode == MAX && arr[i].freq > thres_prof.freq)) {
            thres_prof = arr[i];
            currIdx = i;
        }
    }
    return currIdx;
}

int find_element(IntegerProfile* arr, unsigned int k_int, int val) {
    for(int i = 0; i < k_int; i++) {
        if(arr[i].val == val) return i;
    }
    return -1;
}

void
arrange_list(unsigned int curr_index) {
    int thres_list_idx = find_maxmin(shortlist, k_int, MIN);
    IntegerProfile thres_profile = shortlist[thres_list_idx];
    if(wholelist[curr_index] > thres_profile.freq) {
        int ret = find_element(shortlist, k_int, curr_index);
        if(ret != -1) {
            shortlist[ret].freq = wholelist[curr_index];
        }
        else {
            shortlist[thres_list_idx].val = curr_index;
            shortlist[thres_list_idx].freq = wholelist[curr_index];
        }
    }
}

void* reader(void* arg) {
    CircBuffer* buff = ((CircBuffer**)arg)[0];
    pthread_cond_t* not_full = ((pthread_cond_t**)arg)[1];
    pthread_cond_t* not_empty = ((pthread_cond_t**)arg)[2];
    pthread_mutex_t* buff_lock = ((pthread_mutex_t**)arg)[3];
    char read_str[10];
    while(1) {
        fgets(read_str, 10, stdin);
        if(buff->count == 100) pthread_cond_wait(not_full, buff_lock);
        pushElement(buff, atoi(read_str), NULL);
        pthread_cond_signal(not_empty);
    }
}

void* updater(void* arg) {
    CircBuffer* buff = ((CircBuffer**)arg)[0];
    pthread_cond_t* not_full = ((pthread_cond_t**)arg)[1];
    pthread_cond_t* not_empty = ((pthread_cond_t**)arg)[2];
    pthread_mutex_t* buff_lock = ((pthread_mutex_t**)arg)[3];
    int curr_index;
    while(1) {
        if(buff->count == 0) pthread_cond_wait(not_empty, buff_lock);
        curr_index = popElement(buff, NULL) + 10000;
        pthread_cond_signal(not_full);
        wholelist[curr_index] += 1;
        arrange_list(curr_index);
    }
}

void* printer(void* arg) {
    while(1) {
        sleep(m_rec);
        fprintf(stderr, "ranker: top %d: ", k_int);
        for(int i = 0; i < k_int; i++) {
            int idx = find_maxmin(shortlist, k_int, MAX);
            fprintf(stderr, "%d [%ld] ", shortlist[idx].val, shortlist[idx].freq);
            shortlist[idx].freq = -1;
        }
        fprintf(stderr, "in %d secs\n", m_rec);
        memset(wholelist, 0, sizeof(wholelist));
        memset(shortlist, 0, k_int * sizeof(IntegerProfile));
    }
}

int main(int argc, char** argv) {
    if (argc < 3) {
        printf("Usage: ./ranker <m seconds to record> <k frequent integers>\n");
        return EXIT_FAILURE;
    }
    // stored globally
    m_rec = atoi(argv[1]);
    k_int = atoi(argv[2]);

    shortlist = malloc(k_int * sizeof(IntegerProfile));
    pthread_t reader_pid, updater_pid, printer_pid;
    pthread_cond_t not_empty, not_full;
    pthread_mutex_t buff_lock;
    CircBuffer buff;
    initCircBuffer(&buff);
    
    pthread_cond_init(&not_empty, NULL);
    pthread_cond_init(&not_full, NULL);
    pthread_mutex_init(&buff_lock, NULL);

    void* arg[4] = {&buff, &not_full, &not_empty, &buff_lock};
    pthread_create(&reader_pid, NULL, &reader, arg);
    pthread_create(&updater_pid, NULL, &updater, arg);
    pthread_create(&printer_pid, NULL, &printer, NULL);

    pthread_join(reader_pid, NULL);
    pthread_join(updater_pid, NULL);
    pthread_join(printer_pid, NULL);

    return EXIT_SUCCESS;
}