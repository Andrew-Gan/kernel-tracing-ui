# kernel-research
  
Bootcamps:  
2a. kernel execution profiling  
2b. simple stream processing

Projects:  
Kernel Tracing Visualizer

# Dependencies
python 3.8.2  
matplotlib 3.2.1

# Execution  
`$ python3 main.py [path to trace file]`

This makefile command provides trace files in trace_log/ as input  
`$ make [one | two | three | four]`  

# Configuration                                                                         
| variables     | description                                                  |
|---------------|--------------------------------------------------------------|
| tid_color      | colors for representing operations by different threads     |
| read_color    | color for representing read operations                       |
| write_color   | color for representing write operations                      |
| box_color     | color for filling in rectangles that represent an interval   |
| part_color    | color for partitioning the rows                              |
| dis_group     | registers to display. Use format: (token index, string)      |
| tFilterOn     | if set to true, only traces within tFilter range are plotted |
| tFilter       | range of timestamp to focus on                               |
| rec_group     | registers to record interval (index, name, starting, ending) |

Adding a new register group or name to display.  
`dis_group = ((8, 'CPU_CTRL'), (8, 'JOB_CTRL'))`  
<kbd><img src="img/02.png" width=400 height=200 /></kbd>
  
Multiple patterns can be displayed on the same row as such.  
`dis_group = ((8, 'CTRL', 'CPU_CTRL', 'JOB_CTRL'),)`  
<kbd><img src="img/04.png" width=400 height=200 /></kbd>

Recording the thread id, duration and read/write count of an interval.  
Different colors used for representing different threads.  

Format: (index in tokenized log, name to display on plot, starting register, ending register)  
`rec_group = ((2, 'JIN_PM', 'JIN_PM_START', 'JIN_PM_DONE'))`  
<kbd><img src="img/03.png" width=400 height=200 /></kbd>

# Controls
| key           | action                                                       |
|---------------|--------------------------------------------------------------|
| scroll up     | zoom in                                                      |
| scroll down   | zoom out                                                     |
| A or left arr | scroll left                                                  |
| D or right arr| scroll right                                                 |

# Preview
<kbd><img src="img/preview.png" width=800 height=400 /></kbd>
